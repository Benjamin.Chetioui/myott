# Myott

Make your own type theory!

## About Myott

Myott aims to be a flexible framework for specifying formal systems – such as programming
languages, DSLs, APIs and dependent type theories.

## Current state

The prototype currently supports specifications of dependent judgement forms and operations.

## Developing on Myott

Check out the repository and install the git-hooks:

    $ git clone git@git.app.uib.no:Hakon.Gylterud/myott.git
    $ cd myott
    $ git-hooks/setup.sh

Myott is built using [Cabal](https://www.haskell.org/cabal/):

    $ cabal new-build

This will build the `myott` binary. To check specifications use the `myott check` subcommand:

    $ cabal new-run myott check
    Up to date
    Usage: myott check JUDGEMENT_SPEC TERM_SPEC

Before commiting, the code will be linted and tests will run. To run these manually
use the following shell-scripts:

    $ ./lint.sh
    (⋯)
    $ ./test.sh
    (⋯)

## Using Myott

The prototype is at a stage where only very experimental usage is possible.
Here is an example of the kind of specifications Myott is intended to capture:

![An example specification of an *autocomplete* GUI element.](https://git.app.uib.no/Hakon.Gylterud/myott/-/raw/master/img/autocomplete.svg)
