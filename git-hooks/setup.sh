#!/bin/sh

ln -s $(pwd)/git-hooks/pre-commit .git/hooks || echo "Run this script as \"git-hooks/setup.hs\" from git repo base dir."
ln -s $(pwd)/git-hooks/pre-applypatch .git/hooks || echo "Run this script as \"git-hooks/setup.hs\" from git repo base dir."
