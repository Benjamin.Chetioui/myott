module Sequent.Parser where

import Prelude hiding (init,print)

import Text.Megaparsec
import Text.Megaparsec.Char


import Sequent.AST
import Util.Parser
import Util.Monad




term :: Parser Term
term
  = Term <$> identifier
         <*> arguments (try (Left <$> (identifier <* notFollowedBy (try (space1 <* identifier) 
                                                                    <|> (space <* char '{')))) 
                        <|> (Right <$> term))
                       ((Left <$> identifier) <|> (Right <$> betweenParens term))

judgement :: Parser Judgement
judgement
  = Judgement <$> identifier
              <*> arguments (try (Left <$> (identifier <* notFollowedBy (try (space1 <* identifier) 
                                                                         <|> (space <* char '{')))) 
                             <|> (Right <$> term))
                            ((Left <$> identifier) <|> (Right <$> betweenParens term))

environment :: Parser Environment
environment
  = Environment
   <$> (do
       x <- identifier
       space
       s <- eitherP (char ':') (char '≔')
       space
       p <- either (const (Left <$> judgement)) (const (Right <$> term)) s
       space
       return (x,p)) `sepBy` (char ',' >> space)

sequent :: Parser Sequent
sequent
  = Sequent
   <$> environment
   <*> (char '⊢' >> space >> identifier)
   <*> (space >> identifier `sepBy'` space1)
   <*> (space >> char ':' >> space >> judgement << space << char ';')


specification :: Parser Specification
specification
  = Specification
   <$> many (space *> sequent <* space)
