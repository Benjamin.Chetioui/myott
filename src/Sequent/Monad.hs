module Sequent.Monad where

import Control.Monad.Reader
import Control.Monad.State
import Prelude hiding (init,print)

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import Util.Trace

import qualified Sequent
import qualified Judgement

import Identifier

import Argument.Conventions

type MakeSys s e j t a
    = ReaderT (Sequent.Referee (StateT (Conventions Identifier Identifier Identifier) (TraceError String String)) s e j t)
              (StateT (Conventions Identifier Identifier Identifier) (TraceError String String))
              a

dependency :: Identifier -> Identifier -> e -> MakeSys s e j t Identifier
dependency d v e = ReaderT (\k -> Judgement.varToIdentifier <$> Judgement.dependency (Sequent.context (Sequent.environment k e)) d v)


init :: MakeSys s e j t s
environment :: s -> MakeSys s e j t e
init           = ReaderT (return . Sequent.init)
environment s      = ReaderT (\k -> return $ Sequent.empty k s)
declare n j = ReaderT (\k -> Sequent.declare k n j)
assume n j     = ReaderT (\k -> Sequent.assume k n j)
define n t     = ReaderT (\k -> Sequent.define k n t)
applyForm h as e   = ReaderT (\k -> Sequent.applyForm k h as e)
apply h as c   = ReaderT (\k -> Sequent.apply k h as c)

noArgs :: (Ord a) => Map a b
noArgs = Map.empty

args :: (Ord a) => [(a,b)] -> Map a b
args = Map.fromList

system d = init >>= d
