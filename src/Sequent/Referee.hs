{-# LANGUAGE TupleSections,FlexibleContexts #-}
module Sequent.Referee where

import Prelude hiding (init,head,mapM,sequence)

import Control.Monad.Except
import Control.Monad.Reader

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import qualified Judgement as J
import Judgement.Referee (checkMap,MapError(Unspecified,NotInCodomain,NotInDomain,MissingFromDomainOfDefinition,PartialComposition,IllTyped))
import Sequent

import Util.PrettyPrint
import Util.Trace

check True _ = return ()
check False s = throwWithTrace s

compose' f g = mapM (\b -> case Map.lookup b g of
                            Nothing -> throwWithTrace $ "Could not find '" ++ show b ++ "' in the argument list " ++ show g
                            (Just x) -> return x) f

partialCompose f g = do
   Map.mapMaybe (`Map.lookup` g) f

--checkMapTerms :: Environment -> Environment -> Map Variable Variable -> blah
checkMapTerms' partialOK domain codomain vs =
    let h v t = case Map.lookup v vs of
                     (Just x) -> case Map.lookup x (terms codomain) of
                                   Nothing -> throwWithTrace $ "The assignment '" ++ pprint v ++ " = " ++ pprint x ++ "' fails because '"
                                                     ++ pprint v ++ " := " ++ pprint t ++ "' while '"
                                                  ++ pprint x ++ "' is a variable."
                                   (Just t') -> do
                                                 vs' <- if partialOK then return (partialCompose (arguments t) vs) else compose' (arguments t) vs
                                                 check (t' == Term (head t) vs')
                                                       ("The assignment '" ++ pprint v ++ " = " ++ pprint x ++ "' fails because '"
                                                     ++ pprint t' ++ " ≠ " ++ pprint (Term (head t) vs') ++ "'.")
                     Nothing -> if partialOK then return () else throwWithTrace "Map is partial"
    in tag ("Checking that " ++ show vs ++ " is a valid map from\n Environment " ++ pprint domain ++ "\n   to \n Environment " ++ pprint codomain ) $
          sequence_ $ Map.mapWithKey h (terms domain) -- The map is partial.

checkMapTerms :: (MonadReader [String] m,
                 MonadError ([String],String) m)
              => Environment -> Environment -> Map J.Variable J.Variable -> m ()
checkMapTerms = checkMapTerms' False
checkMapTermsPartial :: (MonadReader [String] m,
                 MonadError ([String],String) m)
              => Environment -> Environment -> Map J.Variable J.Variable -> m ()
checkMapTermsPartial = checkMapTerms' True

protokernel k₀ spec
 = Referee ((fst, fst), fst) id snd snd snd
          -- init
          emptySys
          -- empty
          (, emptyEnv)
          -- declare
          (\n ((s , e), j) -> fmap fst (extSys n e j s))
          -- assume
          (\n ((s , Environment c d r), j) ->
            do
               (_,c') <- J.assume k₀ n ((spec,c),j) -- TODO:  Check that _ match spec
               return (s, Environment c' d r))
          -- Define
          (\n (((s , e), j),t) -> fmap (\(e',_)->(s,e')) (extEnv n j t e))
          -- applyForm
          (\n m (s,e@(Environment c _ _)) ->
            do
               ((_,_),j) <- J.apply k₀ n m (spec,c) -- TODO: Check that _ _ match spec c
               return ((s,e),j))
          -- apply
          (\h m (s,e) ->
            do
               (cst,(env,j)) <- definition h s
               vs <- case checkMap (context env) (context e) m of   -- Verify the substitution
                    (Left (NotInCodomain x)) -> throwWithTrace $ "No variable '" ++ pprint x ++ "' in context '" ++ pprint (context env) ++ "'."
                    (Left (NotInDomain x)) -> throwWithTrace $ "Constant '" ++ pprint h ++"' takes no parameter '" ++ show x ++ "'."
                    (Left (MissingFromDomainOfDefinition x)) ->  throwWithTrace $ "Constant '" ++ pprint h ++"' takes a parameter '"
                                    ++ pprint x ++ "', but none was given."
                    (Left (PartialComposition err)) -> throwWithTrace err
                    (Left (IllTyped err)) -> throwWithTrace $ "Typecheck failed on the arguments of '" ++ pprint h
                             ++ "'.\n  " ++ err
                    (Left (Unspecified err)) -> throwWithTrace err
                    (Right x) -> return x
               mj <- compose' (J.arguments j) vs -- TODO: Do we actually need the next step, or can we construct the new j ourselves.
               let j' = J.Judgement (J.head j) mj (J.kind j)
               -- Now we must check that all constructed terms are properly applied
               checkMapTerms env e vs
               return (((s,e),j'),Term cst vs))
