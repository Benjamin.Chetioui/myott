{-|
    Module      : Sequent.AST
    Description : Abstract syntax trees for operations.
    Stability   : experimental

This module defines abstract syntax trees for operation
specifications and functions to run these on a
'Sequent.Referee'.

The abstract syntax is produced by the parser, but is not
the final product. The abstract syntax is feed to a referee,
which produces a complete combintatorial representation of the
specification.

The heavy lifting in this section is done by 'resolveArguments' and
'constructArguments', which transform the tree structure of the
abstract syntax to a sequential, flattened representation which
is fed through a Referee.

On a syntactic level, what happens is that something like:

@
      x:ob, y:ob,
      f:hom x y,
     ⊢ right-id : ≡ x y (comp x y y f (id y)) f ;
@

is elaborated to:

@
        x:ob, y:ob,
        f:hom x y,
        i ≔ id y ,
        c ≔ comp x y y f i
     ⊢ right-id : ≡ x y c f ;
@

The recursive term @≡ x y (comp x y y f (id y)) f@ is flattened
by introducing intermediate definition (@i@ and @c@).
-}

module Sequent.AST where

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)
import qualified Data.Set as Set

import Control.Monad.Reader
import Control.Monad.State

import qualified Judgement
import qualified Sequent
import Sequent.Monad
import Util.Trace
import Util.PrettyPrint
import Util.Monad
import Argument
import Argument.Conventions
import Argument.Inference

import Identifier


{- | An abstract, syntactic term consists of a head
     (the identifier of the outermost operation in
     the term construction) and a set of arguments,
     which are either names of elements in the environment
     or themselves terms. -}
data Term = Term {
   termHead :: Identifier, {- ^ The the identifier of the outermost operation in
                                the term construction -}
   termArgs :: Arguments Identifier (Either Identifier Term)
                           {- ^ The arguments to the head of the term.-}
   }
   deriving (Eq,Show)

{- | An abstract, syntactic judgement in an environment. -}
data Judgement
    = Judgement Identifier
                (Arguments Identifier (Either Identifier Term))
     deriving (Show)

{- | An abstract, syntactic enviroment consisting of
     assumptions and terms. -}
newtype Environment
  = Environment [(Identifier, Either Judgement Term)]
     deriving (Show)

{- | An abstract, syntactic sequent defining an operation.-}
data Sequent = Sequent  Environment
                        Identifier
                        [Identifier]
                        Judgement
     deriving (Show)

{- | An abstract, syntactic specification. -}
newtype Specification = Specification [Sequent]
     deriving (Show)

-- | Monad stack for running enviroments through a referee.
type MakeEnvironment s e j t a
  = StateT e (ReaderT (Sequent.Referee (StateT (Conventions Identifier Identifier Identifier) (TraceError String String)) s e j t)
                      (StateT (Conventions Identifier Identifier Identifier) (TraceError String String))) a

modifyM f
  = do
     s <- get
     s' <- lift (f s)
     put s'

suffix = map show [0..]

freshest prefix l
 = head
 $ filter (not . (`elem` l))
 $ (prefix <+<) <$> suffix


-- | Produce a fresh variable in an environment based on a prefix.
fresh :: Identifier -- ^ A prefix for the fresh variable.
      -> e          -- ^ The current environment.
      -> MakeSys s e j t Identifier
fresh prefix e = do
   env <- asks Sequent.environment <*> pure e
   return
     $ freshest prefix
                (Judgement.varToIdentifier <$> Map.keys (Sequent.terms env))


-- | Checks if a given term construction already exists in an environment.
checkConstructed :: Identifier -- ^ The identifier of the operation.
                 -> Map Identifier Identifier -- ^ The arguments to the operation.
                 -> e -- ^ The current environment
                 -> MakeSys s e j t (Maybe Identifier) -- ^ An identifier for the term if already defined.
checkConstructed h as e =
 do
  env <- asks Sequent.environment <*> pure e
  -- TODO: Sort this mess of converting from Constant and Variable to
  --       identifier.
  let m = Map.mapKeysMonotonic (\t-> (Sequent.constantToIdentifier (Sequent.head t) , Map.mapKeysMonotonic Judgement.varToIdentifier $ Map.map Judgement.varToIdentifier $ Sequent.arguments t)) (Sequent.representation env) :: Map (Identifier, Map Identifier Identifier) Judgement.Variable
  return $ Judgement.varToIdentifier <$> Map.lookup (h,as) m

{- | Construct a term from its head and arguments through a Referee.
     The juice of 'construct' is that it only runs through the Referee
     if the term is not already constructed. -}
construct :: Identifier  -- ^ The identifier of the operation.
          -> Map Identifier Identifier  -- ^ The arguments to the operation
          -> MakeEnvironment s e j t Identifier  -- ^ The identifier of the constructed term.
construct h is
 = do
     e <- get
     v <- lift $ checkConstructed h is e
     maybe (do
             i <- lift $ fresh h e
             modifyM (define i <^ apply h is)
             return i)
           return v


-- | Recursively construct all the arguments of an operation.
constructArguments :: Identifier -- ^ The identifier of the operation.
                   -> Arguments Identifier (Either Identifier Term)  -- ^ The recursive arguments of the operation.
                   -> MakeEnvironment s e j t (Map Identifier Identifier)
                   {- ^ The flattened arugments of the operation, now constructed
                        in the current context.-}
constructArguments h as
 = do
    ts <- lift $ lift (nameArguments h as)
    is <- forM ts (either return
                    (\t -> do
                      is' <- constructArguments (termHead t) (termArgs t)
                      construct (termHead t) is'))
    e <- get
    env <-  lift $ asks $ ($ e) . Sequent.environment
    let context = Judgement.dependencies (Sequent.context env)
    lift $ lift $ inferArguments context h is

runTerm :: Term -- ^ The term to run through a referee.
        -> e    -- ^ The current environment
        -> MakeSys s e j t t -- ^ The refereed term.
runTerm (Term h as) e
 = do
    (is, e') <- runStateT (constructArguments h as) e
    apply h is e'

runJudgement :: Judgement  -- ^ The judgement to construct
             -> e          -- ^ The current enviroment
             -> MakeSys s e j t j
                           -- ^ The refereed judgement
runJudgement (Judgement h as) e
 = do
    (is, e') <- runStateT (constructArguments h as) e
    applyForm h is e'

runEnvironment :: Environment  -- ^ The enviroment to construct
               -> s            -- ^ The current specification
               -> MakeSys s e j t e
                               -- ^ The refereed enviroment
runEnvironment (Environment ts)
  = environment
     ^> chain (map (\(i , k)
            -> either (\j -> assume i <^ runJudgement j)
                      (\t -> define i <^ runTerm t) k) ts)
runSequent :: Sequent
           -> s
           -> MakeSys s e j t s
runSequent (Sequent env name arguments judgement) s
  = mapReaderT (tag ("In the definition of '" ++ pprint name ++ "':"))
  $ do
      e <- runEnvironment env s
      env' <- asks $ ($ e) . Sequent.environment
      let context = Judgement.dependencies (Sequent.context env')
      j <- runJudgement judgement e
      s' <- declare name j
      let res = createResolution context (Set.fromList arguments)
      modify (insertConvention name arguments res)
      return s'


runSpecification :: Specification
                      -- ^ The specification to run through the referee
                 -> MakeSys s e j t s
                      -- ^ The refereed specification
runSpecification (Specification ss)
  = system $ chain (map runSequent ss)
