{-# LANGUAGE FlexibleContexts #-}
module Theory.Monad where

import Control.Monad.Reader

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import qualified Sequent
import qualified Theory
import qualified Judgement
import Util.Trace
import Util.PrettyPrint

type MakeTheory tt f e j a t x =  ReaderT (Theory.Referee (TraceError String String) tt f e j a t) (TraceError String String) x

dependency d v e =
  ReaderT (\k -> Judgement.varToIdentifier <$> Judgement.dependency (Sequent.context (Sequent.environment k e)) d v)


theory :: MakeTheory tt f e j a t tt
frame :: tt -> MakeTheory tt f e j a t f
environment :: f -> MakeTheory tt f e j a t e
theory            = ReaderT (return . Theory.initTheory)
frame tt          = ReaderT (\k -> return $ Theory.initFrame k tt)
environment f     = ReaderT (\k -> return $ Theory.emptyEnv k f)
constant n a      = ReaderT (\k -> tag ("Defining constant " ++ pprint n ++ ".")
                                 $ Theory.defineInFrame k n a)
parameter n j     = ReaderT (\k -> tag ("Declaring parameter " ++ pprint n ++ ".")
                                 $ Theory.declareConstant k n j)
rule n j   = ReaderT (\k -> tag ("Defining rule " ++ pprint n ++ ".")
                                 $ Theory.declare k n j)
variable n j      = ReaderT (\k -> tag ("Assuming the variable " ++ pprint n ++ ".")
                                 $ Theory.assumeVariable k n j)
term n t          = ReaderT (\k -> tag ("Assuming the variable " ++ pprint n ++ ".")
                                 $ Theory.defineInEnv k n t)
form h as e       = ReaderT (\k -> tag ("Applying the judgement form " ++ pprint h ++ ".")
                                 $ Theory.applyForm k h as e)
applyTerm h a c   = ReaderT (\k -> tag ("Applying the constant " ++ pprint h ++ ".")
                                 $ Theory.applyConstant k h a c)
applyRule h a r c = ReaderT (\k -> Theory.applyRule k h a r c)

noArgs :: (Ord a) => Map a b
noArgs = Map.empty

args :: (Ord a) => [(a,b)] -> Map a b
args = Map.fromList





{-
declareZero :: s -> MakeSys s e j t s
declareZero = declare "0" noArgs <^

-}
