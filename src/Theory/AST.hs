{- |
    Module      : Theory.AST
    Description : Abstract syntax trees for theories.
    Stability   : experimental

This module defines abstract syntax trees for
theory specifications and functions to run these
on a 'Theory.Referee'.

The abstract syntax is produced by the parser, but
is not the final product. The abstract syntax is
fed to a referee, which produces a complete
combintatorial representation of the theory.
-}

module Theory.AST where

import Identifier
import Sequent.AST (Judgement, Term, Environment)


{- | An abstract, syntactic theory consists of
     a list of rules. -}
newtype Theory = Theory [Rule]
     deriving (Show)

{- | An abstract, syntactic rule consists of a
     frame (that which is 'above the line') and a
     sequent, but where the conclusion cannot be a
     term (that which is 'below the line'). -}
data Rule = Rule Frame Environment Identifier [Identifier] Judgement
     deriving (Show)

{- | An abstract, syntactic frame consists
     of a list of sequents. -}
newtype Frame = Frame [Sequent]
     deriving (Show)

{- | An abstract, syntactic sequent consists of an
     environment (that which is to the left of \'⊢\'), and
     an identifier (with possible arguments) and either a
     judgement or a term (that which is to the right of
     \'⊢\'). -}
data Sequent = Sequent Environment Identifier [Identifier] (Either Judgement Term)
     deriving (Show)
