{-|
    Module      : Theory.Parser
    Description : Parser for theory specifications.
    Stability   : experimental

This module defines parsers for concepts related
to theories.
-}

module Theory.Parser where

import Text.Megaparsec
    -- ( optional, eitherP, many, some, MonadParsec(try) )
import Text.Megaparsec.Char ( char, space, space1 )

import Sequent.Parser ( judgement, term, environment )
import Theory.AST
import Util.Parser


-- | Parse a theory specification, which is a
--   whitespace separated list of rules.
theory :: Parser Theory
theory = Theory <$> many (space *> rule <* space)

-- | Parse a rule. The following is an example of
--   a rule:
-- 
-- @
--   {
--     ⊢ A : Type ;
--     x : Element A ⊢ B : Type
--     ────────────────────────
--     ⊢ Π : Type
--   }
-- @
rule :: Parser Rule
rule = betweenCurlies $ do
    fr <- frame
    space >> some (char '─') >> space
    env <- environment
    space >> char '⊢' >> space
    ident <- identifier
    space
    args <- identifier `sepBy'` space1
    space >> char ':' >> space
    Rule fr env ident args <$> judgement

-- | Parse a frame. The following is an example of
--   a frame:
--
-- @
--   ⊢ A : Type ;
--   x : Element A ⊢ B : Type
-- @
frame :: Parser Frame
frame = do
    sequents <- sequent `sepBy'` (space >> char ';' >> space)
    optional $ try (space >> char ';')
    return $ Frame sequents

-- | Parse a sequent. The following is an example
--   of a sequent:
--
-- @
--   x : Element A ⊢ B : Type
-- @
--
--  Another example is:
--
-- @
--     ⊢ p ≔ Π A B
-- @
sequent :: Parser Sequent
sequent = do
    env <- environment
    space >> char '⊢' >> space
    ident <- identifier
    space
    args <- identifier `sepBy'` space1
    space
    sep <- eitherP (char ':') (char '≔')
    space
    val <- either (const (Left <$> judgement)) (const (Right <$> term)) sep
    return $ Sequent env ident args val
