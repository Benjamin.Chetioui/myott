{-# LANGUAGE FlexibleContexts, TupleSections #-}
module Theory.Referee where

import Util.PrettyPrint
import Util.Trace
import Prelude hiding (head)

import qualified Judgement as J
import qualified Judgement.Referee as JK
import Judgement (Judgement,Variable)
import Judgement.Referee (checkMap)
import Sequent (Constant)
import qualified Sequent as S
import qualified Sequent.Referee as SK
import qualified Data.Map.Strict as Map
import Data.Map (Map)
import qualified Data.Set as Set
import Control.Monad.Except
import Control.Monad.Reader

import Identifier

import Theory

mapLeft f (Left x) = throwWithTrace $ f x
mapLeft _ (Right y) = return y


compose'' f g = do
   mapM (\(b,(vs,bs)) -> case Map.lookup b g of
                            Nothing -> throwWithTrace $ "Could not find '" ++ show b
                                            ++ "' in the argument list " ++ show g
                            (Just (c,(vs',bs'))) -> do
                                   vs'' <- SK.compose' vs' vs
                                   bs'' <- SK.compose' bs' bs
                                   return (c,(vs'',bs''))) f

checkPartialMap (J.Context s r) ctx' a
 = checkMap (J.Context (Map.filterWithKey (\v _ -> J.varToIdentifier v `Set.member` Map.keysSet a) s)
                       (Map.map (Set.filter ((`Set.member` Map.keysSet a) . J.varToIdentifier)) r))
            ctx'
            a

image = Set.fromList . map snd . Map.toList



mapConstants m (S.Environment c t _) =  do
  t' <- mapM mt t
  return $ S.Environment c t' undefined where
   mt (S.Term h as) = case Map.lookup h m of
         (Just (h',_)) -> return (S.Term h' as)
         Nothing -> throwWithTrace $ "The map " ++ show m
           ++ " does not contain a value for constant " ++ pprint h ++ "."

checkMapRule :: (MonadReader [String] m,
                 MonadError ([String],String) m)
             =>  Frame -> Frame -> Map Identifier (Identifier, (Map Identifier Identifier, Map Identifier Identifier))
             -> S.Environment
             -> m (Map Constant (Constant, (Map Variable Variable,Map Variable Variable)))
checkMapRule domain codomain m hyp =
 tag ("Checking the validity of the frame map " ++ show (Map.toList $ Map.map fst m)) $
  do
    m' <- tag "Checking that the maps are valid at the level context" $
      sequence $ Map.mapWithKey (\h (h',(a,b)) ->
      tag ("Checking that the assignment '" ++ pprint h ++ " = " ++ pprint h'
                ++ unwords (map (\(x,y) ->"(" ++ pprint x ++ " = " ++ pprint y ++ ")") $ Map.toList a)
                ++ " preserves contex structure.") $
       do
          (c, (S.Environment ctx _ _,_)) <- S.definition h (system domain)
          (c', (S.Environment ctx' _ _,_)) <- S.definition h' (system codomain)
          vs <- mapLeft (\_ -> "the map " ++ show a
                            ++ " is not a valid context map from "
                            ++ pprint ctx' ++ " to " ++ pprint ctx)
                        $ checkPartialMap ctx' ctx a
          bs <- mapLeft (\_ -> "the map " ++ show b
                            ++ " is not a valid context map from "
                            ++ pprint (S.context hyp) ++ " to " ++ pprint ctx)
                        $ checkPartialMap (S.context hyp) ctx' b
          return (c,(c',(vs,bs)))) m
    let mj = Map.fromAscList $ snd <$> Map.toAscList m'
    tag "Checking that rules and constants are coherently applied." $
     sequence_ $ Map.mapWithKey (\c (c',(vs,bs)) ->
      tag ("Checking that the assignment '" ++ pprint c ++ " = " ++ pprint c'
                ++ unwords (map (\(x,y) ->"(" ++ pprint x ++ " = " ++ pprint y ++ ")") $ Map.toList vs)
                ++ " is well typed.") $
       do
          let (S.Constant h) = c
          let (S.Constant h') = c'
          (_, (e,j)) <- S.definition h (system domain)
          (_, (e',j')) <- S.definition h' (system codomain)
          -- Check that the maps are valid for environments
          e'' <- mapConstants (Map.map (\(x,(y,_)) -> (x,y)) mj) e
          tag ("Checking that premises of " ++ pprint c'
                 ++ " are correctly translated to premises of " ++ pprint c) $
           SK.checkMapTermsPartial e' e'' vs
          tag ("Checking that the conclusion is coherent wrt. premise " ++ pprint c') $
           SK.checkMapTermsPartial hyp e' bs
          let cover = (Map.keysSet vs `Set.union`
                       Map.keysSet (S.terms e'))`Set.union`
                       image bs
          tag "Checking that the hypothesis of premises are not too general" $
           case Set.lookupMin (Map.keysSet (J.structure $ S.context e') `Set.difference` cover) of
              Nothing -> return ()
              (Just v) -> throwWithTrace $ "Variable not instanciated:" ++ pprint v
          vs' <- SK.compose' (J.arguments j') vs
          if J.Judgement (J.head j') vs' (J.kind j') == j
            then return ()
            else throwWithTrace $ "The judgement form of " ++ pprint c
                                ++ " is not compatible with the judgement form of " ++ pprint c'
                                ++ ": \n" ++ pprint (J.Judgement (J.head j') vs' (J.kind j'))
                                ++ "\n   ≠\n" ++ pprint j
          tag "Checking that the maps are valid for applications of previous rules." $
           case (Map.lookup c (applications domain), Map.lookup c' (applications codomain)) of
            (Nothing, _) -> return ()
            (Just app, Nothing) -> throwWithTrace $ "The constant '" ++ pprint c ++ " := " ++ pprint app
                                  ++ "' is mapped to the free parameter '" ++ pprint c' ++ ".'"
            (Just app, Just app') ->
                  do
                    mj' <- compose'' (parameters app') mj
                    vs'' <- SK.compose' (arguments app') vs
                    if Application (head app') mj' vs'' == app
                      then return ()
                      else throwWithTrace "Error") mj
    return mj

dummykernel spec = Referee {
    deps = ((((fst,fst), fst), fst), fst),
    theory = id,
    frame = snd,
    environment = snd,
    judgement = snd,
    term = snd,
    application = snd,
    initTheory = Theory Map.empty,
    initFrame = (, Frame S.emptySys Map.empty Map.empty),
    emptyEnv = (,S.emptyEnv),

    defineInFrame = \h ((((tt,f),e),j),a)
       -> (tt,) . fst <$> extFrameDef h e j a f,

    declareConstant = \h (((tt,Frame s m r),e),j)
       -> (\(s',_) -> (tt,Frame s' m r)) <$> S.extSys h e j s,

    assumeVariable = \h ((f,S.Environment c d r),j)
       -> (\(c',_) -> (f, S.Environment c' d r)) <$> J.extCtx h j c,

    defineInEnv =  \h (((f,e),j),t)
       -> (f,) . fst <$> S.extEnv h j t e,

    declare = \h (((t,f),e),j)
       -> fmap fst (extTheory h f e j t),

    applyForm = \n m (s,e@(S.Environment c _ _)) ->
            do
               ((_,_),j) <- J.apply k₀ n m (spec,c) -- TODO: Check that _ _ match spec c
               return ((s,e),j),
    applyConstant = \h m (f@(_,Frame s _ _),e@(S.Environment c _ _)) ->
            do
               (cst,(env@(S.Environment ctx _ _),j)) <- S.definition h s
               vs <- case checkMap ctx c m of   -- Verify the substitution
                    Left _ -> throwWithTrace "Something wrong." -- TODO: unpack these errors
                    Right x -> return x
               SK.checkMapTerms env e vs
               mj <- SK.compose' (J.arguments j) vs
               let j' = J.Judgement (J.head j) mj (J.kind j)
               return (((f,e),j'),S.Term cst vs),
    applyRule = \h m₀ m₁ ((tt,f),e) -> tag ("Applying the rule " ++ pprint h ++ ".") $
             do
                ((f',e'),j) <- definition h tt
                m₀' <- tag ("Checking the parameters of " ++ pprint h ++ ".")
                           (checkMapRule f' f m₀ e)
                m₁' <- mapLeft (const "Error" ) $
                               checkMap (S.context e') (S.context e) m₁
                SK.checkMapTerms e' e m₁'
                as <- SK.compose' (J.arguments j) m₁'
                let j' = J.Judgement (J.head j) as (J.kind j)
                return ((((tt,f),e),j'), Application (Rule h) m₀' m₁') } where
 k₀ ::  (MonadReader [String] m,
                 MonadError ([String],String) m)
    => J.Referee
                m
                J.Specification
                (J.Specification, J.Context)
                ((J.Specification, J.Context), Judgement)
 k₀ = JK.protokernel
