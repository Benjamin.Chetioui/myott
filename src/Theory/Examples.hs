{-# LANGUAGE FlexibleContexts #-}
module Theory.Examples where

import Prelude hiding ((<>))
import qualified Data.Map.Strict as Map
import Identifier
import Util.Monad

import Theory.Monad hiding (constant, parameter, rule, variable, term, form, applyTerm , applyRule)
import qualified Theory.Monad as M

constant = M.constant . Identifier
parameter = M.parameter . Identifier
rule = M.rule . Identifier
variable = M.variable . Identifier
term = M.term . Identifier
form n m = M.form (Identifier n)
                (Map.map Identifier (Map.mapKeysMonotonic Identifier m))
applyTerm n m = M.applyTerm (Identifier n)
                (Map.map Identifier (Map.mapKeysMonotonic Identifier m))


applyRule n m r = M.applyRule (Identifier n)
         (Map.map (\(i , (m' , m'')) -> (Identifier i , (Map.map Identifier (Map.mapKeysMonotonic Identifier m') , Map.map Identifier (Map.mapKeysMonotonic Identifier m'')))) (Map.mapKeysMonotonic Identifier m)) (Map.map Identifier (Map.mapKeysMonotonic Identifier r))

ttype = form "type" noArgs
element t = form "element" (args [("A",t)])

declareΠ :: tt -> MakeTheory tt f e j a t tt
declareΠ
  = rule "Π" <^ (frame ^>
                  parameter "A" <^ ttype
                                <^ environment ^>
                  parameter "B" <^ ttype <^ variable "x" <^ element "A"
                                <^ (environment ^>
                                   (term "A" <^ applyTerm "A" noArgs)) ^>
                  environment ^> ttype)

π a x b a' = applyRule "Π" (args [("A",(a,(noArgs,noArgs))),("B",(b,(args [(a',"A"),(x,"x")],noArgs)))]) noArgs

declareλ :: tt -> MakeTheory tt f e j a t tt
declareλ
  = rule "λ" <^ (frame ^>
                  parameter "X" <^ ttype
                                <^ environment ^>
                  parameter "Y" <^ ttype <^ variable "x" <^ element "A"
                                <^ (environment ^>
                                   (term "A" <^ applyTerm "X" noArgs)) ^>
                  constant "ΠXY" <^ π "X" "x" "Y" "A" <^ environment ^>
                  parameter "y" <^ element "Y"
                                <^ (environment ^>
                                    term "X" <^ applyTerm "X" noArgs ^>
                                    variable "x" <^ element "X"^>
                                    term "Y" <^ applyTerm "Y" (args [("A","X"),("x","x")])) ^>
                  environment ^>
                  term "ΠXY" <^ applyTerm "ΠXY" noArgs ^>
                  element "ΠXY")

declareApp :: tt -> MakeTheory tt f e j a t tt
declareApp
  = rule "app" <^ (frame ^>
                  parameter "A" <^ ttype
                                <^ environment ^>
                  parameter "B" <^ ttype <^ variable "x" <^ element "A"
                                <^ (environment ^>
                                   (term "A" <^ applyTerm "A" noArgs)) ^>
                  constant "ΠAB" <^ π "A" "x" "B" "A" <^ environment ^>
                  parameter "f" <^ element "ΠAB"
                                <^ (environment ^>
                                    term "ΠAB" <^ applyTerm "ΠAB" noArgs) ^>
                  environment ^>
                  term "A" <^ applyTerm "A" noArgs ^>
                  term "ΠAB" <^ applyTerm "ΠAB" noArgs ^>
                  variable "f" <^ element "ΠAB" ^>
                  variable "x" <^ element "A" ^>
                  term "B" <^ applyTerm "B" (args [("A","A"),("x","x")]) ^>
                  element "B")




declareℕ :: tt -> MakeTheory tt f e j a t tt
declareℕ
  = rule "ℕ" <^ ttype <^ environment <^ frame

nat = applyRule "ℕ" noArgs noArgs

declareSucc :: tt -> MakeTheory tt f e j a t tt
declareSucc
  = rule "succ" <^ element "ℕ" <^ (environment ^>
                                   term "ℕ" <^ applyTerm "ℕ" noArgs ^>
                                   variable "x" <^ element "ℕ" )
                               <^ (frame ^>
                                   constant "ℕ" <^ nat <^ environment)



doublePi :: tt -> MakeTheory tt f e j a t f
doublePi =
  frame ^>
     parameter "A" <^ ttype
                   <^ environment ^>
     parameter "B" <^ ttype
                   <^ variable "x" <^ element "A"
                   <^ term "A" <^ applyTerm "A" noArgs
                   <^ environment ^>
     parameter "C" <^ ttype
                   <^ (environment ^>
                       term "A" <^ applyTerm "A" noArgs ^>
                       variable "x" <^ element "A" ^>
                       term "B" <^ applyTerm "B" (args [("A","A"),("x","x")]) ^>
                       variable "y" <^ element "B") ^>
     constant "ΠBC" <^ applyRule "Π" (args [("A",("B",(noArgs,args [("A","A"),("x","x")]))),("B",("C",(args [("B","A"),("y","x")],args [("A","A"),("x","x")])))]) noArgs
                    <^ (environment ^>
                       term "A" <^ applyTerm "A" noArgs ^>
                       variable "x" <^ element "A") ^>
     constant "ΠAΠBC" <^ applyRule "Π" (args [("A",("A",(noArgs,noArgs))),("B",("ΠBC",(args [("A","A"),("x","x")],noArgs)))]) noArgs
                    <^ environment
