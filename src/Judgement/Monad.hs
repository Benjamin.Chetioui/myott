module Judgement.Monad where

import Data.Map.Strict (Map)

import Control.Monad.Except
import Control.Monad.State

import Util.Trace

import qualified Judgement as JF
import Prelude hiding (init,print)
import Control.Monad.Reader

import Identifier
import Argument.Conventions

type MakeSpec s c j a
    = ReaderT (JF.Referee (StateT (Conventions Identifier Identifier Identifier) (TraceError String String )) s c j)
              (StateT (Conventions Identifier Identifier Identifier) (TraceError String String ))
              a


init :: MakeSpec s c j s
context :: s -> MakeSpec s c j c
declare :: Identifier -> JF.JudgementKind -> c -> MakeSpec s c j s
assume :: Identifier -> j -> MakeSpec s c j c
apply :: Identifier -> Map Identifier Identifier -> c -> MakeSpec s c j j

init           = ReaderT (return . JF.init )
context s      = ReaderT (\k -> return $ JF.empty k s)
declare n jk c = ReaderT (\k -> JF.declare k n jk c)
assume n j     = ReaderT (\k -> JF.assume k n j)
apply h as c   = ReaderT (\k -> JF.apply k h as c)

specification d = init >>= d

sort = JF.Sort
proposition = JF.Proposition
