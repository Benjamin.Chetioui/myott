{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Judgement.AST
    Description : Abstract syntax trees for judgement forms.
    Stability   : experimental

This module defines abstract syntax trees for judgement
form specifications and functions to run these on a
'Judgement.Referee'.

The abstract syntax is produced by the parser, but is not
the final product. The abstract syntax is feed to a referee,
which produces a complete combintatorial representation of the
specification.
-}
module Judgement.AST where

import qualified Judgement
import Judgement.Monad

import qualified Data.Set as Set

import Control.Monad.Reader
import Control.Monad.State

import Identifier
import Util.Trace
import Util.PrettyPrint
import Util.Monad
import Argument
import Argument.Inference
import Argument.Conventions


{- | An abstract, syntactic judgment (ex: @Hom x y@) is a
     judgement identifier (ex: @Hom@) with identifiers as
     arguments (ex: @x@ and @y@). -}
data Judgement = Judgement Identifier (Arguments Identifier Identifier)

{- | An abstract, syntactic declaration (ex: @f : Hom x y@)
     is an identifier being declared (ex: @f@) along with
     a judgement it witnesses (ex: @Hom x y@).-}
data Declaration = Declaration Identifier Judgement

{- | An abstract, syntactic context
     (ex: @x : Ob , y : Ob, f : Hom x y@) is a
     list of declarations.) -}
newtype Context = Context [Declaration]

{- | An abstract, syntactic sequent defining a judgement form
     (ex: @x : Ob , y : Ob ⊢ Hom sort@) consists of a context,
     an identifier (ex: @Hom@) and a kind (@Sort@ or @Proposition@) -}
data Sequent = Sequent Context
                       Identifier
                       [Identifier]
                       Judgement.JudgementKind

{- | An abstract, syntactic specification of judgement forms
     is a list of sequents. -}
newtype Specification = Specification [Sequent]


{- Functions for running AST specification on a referee -}

{- | Run a abstract, syntactic 'Judgement' through a 'Referee' -}
runJudgement :: Judgement   -- ^ The judgement to verify.
             -> c           -- ^ The current context.
             -> MakeSpec s c j j
runJudgement (Judgement i as) c
 = do
    combinatorialContext <- asks $ ($ c) . Judgement.context
    let cxt = Judgement.dependencies combinatorialContext
    ns <- lift $ nameArguments i as
    is <- lift $ inferArguments cxt i ns
    apply i is c

{- | Run an abstract, syntactic 'Declaration' through a 'Referee'
     expanding the current context with a new variable. -}
runDeclaration :: Declaration -- ^ The declaration to verify
               -> c           -- ^ The current context
               -> MakeSpec s c j c
runDeclaration (Declaration i j)
   = assume i <^ runJudgement j

{- | Run an abstract, syntactic 'Context' through a 'Referee'
     Constructing it step by step. -}
runContext :: Context     -- ^ The context to verify
           -> s           -- ^ The current specification
           -> MakeSpec s c j c
runContext (Context ds)
   = context ^> chain (map runDeclaration ds)

{- | Run an abstract, syntactic 'Sequent' through a 'Referee'
     Constructing first its left-hand-side context then
     declaring the new judgement form in the current specification. -}
runSequent :: Sequent     -- ^ The sequent defining the judgement
           -> s           -- ^ The current specification
           -> MakeSpec s c j s
runSequent (Sequent c i conv k) s
   = mapReaderT (tag ("In the definition of '" ++ pprint i ++ "':"))
        $ do
            ctx <- runContext c s
            combinatorialContext <- asks $ ($ ctx) . Judgement.context
            s' <- declare i k ctx
            -- Lookup variable dependencies
            let res = createResolution (Judgement.dependencies combinatorialContext) (Set.fromList conv)
            modify (insertConvention i conv res)
            return s'

{- | Run an abstract, syntactic 'Specification' through a 'Referee'
     Constructing first its left-hand-side context then
     declaring the new judgement form in the current specification. -}
runSpecification :: Specification -- ^ The specification to run
                 -> MakeSpec s c j s
runSpecification (Specification ss)
 = specification $ chain (map runSequent ss)
