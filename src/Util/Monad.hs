module Util.Monad where

infixl 1 ^>
infixl 2 <^

(<^) :: Monad m => (b -> m c) -> (a -> m b) -> (a -> m c)
f <^ g = \a -> do
    b <- g a
    f b

(^>) :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)
(^>) = flip (<^)

chain :: Monad m => [a -> m a] -> a -> m a
chain = foldr (^>) return

(<<) :: (Monad m) => m a -> m b -> m a
f << g = do
    x <- f
    g
    return x
