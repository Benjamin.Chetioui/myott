{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Argument.Conventions
    Description : Calling conventions for arguments.
    Stability   : experimental

This module defines...
-}
module Argument.Conventions where

import Argument
import Argument.Inference
import Util.Trace
import Util.PrettyPrint

import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import Control.Monad.Reader
import Control.Monad.Except
import Control.Monad.State


{- | A set of calling conventions for each kind
     of judgment. The conventions specifies the
     order of the named arguments to each form
     for judgments. Example: @Hom@ would be
     assigned @[domain , codomain]@ to specify
     that @Hom x y@ means that @x@ is the domain
     and @y@ the codomain.-}
data Conventions object argname index
    = Conventions {
        argumentSequence :: Map object [argname],
        resolution :: Map object (Resolution Maybe argname index)}
  deriving (Eq,Show)

emptyConventions :: Conventions object argname index
emptyConventions = Conventions Map.empty Map.empty

instance (Ord object)=> Semigroup (Conventions object argname index) where
    (Conventions as r) <> (Conventions as' r')
        = Conventions (as <> as') (r <> r')

instance (Ord object)=> Monoid (Conventions object argname index) where
    mempty = Conventions Map.empty Map.empty

-- | Insert conventions for a specific object
insertConvention :: (Ord object)
                 => object
                 -> [argname]
                 -> Resolution Maybe argname index
                 -> Conventions object argname index
                 -> Conventions object argname index
insertConvention a args res (Conventions as r)
    = Conventions (Map.insert a args as)
                  (Map.insert a res r)

-- | Infer arguments given of a call.
inferArguments :: (Ord source,    PrettyPrint source
                  ,Ord argname,   PrettyPrint argname
                  ,Ord target,    PrettyPrint target
                  ,Ord index,     PrettyPrint index
                  ,MonadReader [String] m
                  ,MonadError ([String],String) m
                  ,MonadState
                    (Conventions source argname index) m)
               => DependencyRelation target index
               -- ^ Current context
               -> source
               -- ^ Term head
               -> NamedArguments argname target
               -- ^ Arguments to the term
               -> m (NamedArguments argname target)
               -- ^ Resolved arguments
inferArguments cxt i named
 =  do
     conventions <- get
     case Map.lookup i (resolution conventions) of
        Just res -> do
                ref <- mergeArguments named res
                inferAll cxt ref
        Nothing -> return named

-- | Assign names to arguments in argument lists.
nameArguments :: (Ord source,    PrettyPrint source
                 ,Ord argname,   PrettyPrint argname
                 ,Ord index,     PrettyPrint index
                 ,MonadReader [String] m
                 ,MonadError ([String],String) m
                 ,MonadState
                    (Conventions source argname index) m)
                 => source
                 -- ^ Term head
                 -> Arguments argname target
                 -- ^ Arguments to the term
                 -> m (NamedArguments argname target)
                 -- ^ Resolved arguments
nameArguments i (ArgList is)
  = tag ("When assigning names to arguments of '" ++ pprint i ++ "':")
  $ do
    conventions <- get
    k <- force  (Map.lookup i (argumentSequence conventions))
                ("The name '"
                    ++ pprint i
                    ++ "' does not have any associated calling convention.")
    if length k == length is
       then return $ Map.fromList $ zip k is
       else throwWithTrace $ "The judgement '" ++ pprint i
                                ++ "' takes " ++ show (length k) ++ " arguments,"
                                ++ " but was given " ++ show (length is) ++ "."
nameArguments _ (ArgMap is) = return is
