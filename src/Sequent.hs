{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Sequent
    Description : Data types for operation specifications and interface for referee.
    Stability   : experimental

This module defines a combinatorial representation of
a operation specification consisting of sequents, as
well as an interface, 'Sequent.Referee',
to safely construct such specifications.
-}
module Sequent where

import Prelude hiding (init,head)
import qualified Data.Map.Strict as Map
import Data.Map.Strict (Map)

import qualified Judgement as J
import Judgement (Context(Context),Variable,Judgement,emptyCtx)
import Util.PrettyPrint
import Util.Trace
import Identifier

{-| A 'Constant' is the name for an operation in
    a specification. -}
newtype Constant = Constant Identifier
  deriving (Eq,Ord,Show)


constantToIdentifier :: Constant -> Identifier
constantToIdentifier (Constant i) = i

{- | A 'Term' is an application of an operation to
     a list of arguments.-}
data Term =
  Term {
    head :: Constant,
    arguments :: Map Variable Variable
  }
  deriving (Eq,Ord,Show)

{- | An environment is a collection of elements, each of which are either
     assumptions or applications of operations. -}
data Environment =
  Environment {
       -- | The underlying context, spefying the judments forms of the elements.
       context :: Context,
       {- | The 'terms' map maps those elements which
            are constructed from operations the specification
            of their construction. -}
       terms :: Map Variable Term,
       {- | A reverse mapping giving you the variable form a
       specific application. -}
       representation :: Map Term Variable }
  deriving (Eq,Ord,Show)

-- | A system is complete specification of a set of operations.
newtype System =
  System (Map Constant (Environment,Judgement))
  deriving (Eq,Ord,Show)

{- | 'Sequent.Referee' is an interface for safely constructing
specifications of operations, aka 'System's. -}
data Referee m s e j t =
  Referee {
    {- | Navigate up in the speficiation along
         the dependencies: Terms have an underlying judgement,
         judgements lie in an environment and environments are
         relative to a specification.-}
    deps          :: ((t -> j, j -> e), e -> s),
    {- | Realise the concrete 'System'. -}
    specification :: s -> System,
    {- | Realise the concrete 'Environment'. -}
    environment   :: e -> Environment,
    {- | Realise the concrete 'Judgement'. -}
    judgement     :: j -> Judgement,
    {- | Realise the concrete 'Term'. -}
    term          :: t -> Term,
    {- | Initialise a specification. -}
    init          :: s,
    {- | Start an empty context. -}
    empty         :: s -> e,
    {- | Declare a new operation. -}
    declare       :: Identifier -> j -> m s,
    {- | Assume a new variable. -}
    assume        :: Identifier -> j -> m e,
    {- | Define a term in an environment. -}
    define        :: Identifier -> t -> m e,
    {- | Form a new judement in an environment by applying a judgement form
         to some arguments. -}
    applyForm     :: Identifier -> Map Identifier Identifier -> e -> m j,
    {- | Form a new term in an environment by applying an operation
         to some arguments. -}
    apply         :: Identifier -> Map Identifier Identifier -> e -> m t
  }

type Declaration m s e j t = Referee m s e j t -> s -> m s
type Construction m s e j t = Referee m s e j t -> e -> m e


{- Utilitiy functions -}

emptyEnv = Environment emptyCtx Map.empty Map.empty

--extEnv :: String -> Judgement -> Term -> Environment -> m Environment
extEnv n j t e@(Environment c d r) = do
    (c',v) <- J.extCtx n j c
    case Map.lookup t r of
      Nothing -> return (Environment c' (Map.insert v t d) (Map.insert t v r), v)
      (Just v') -> throwWithTrace
                    $ "Cannot assign '" ++ pprint n ++ " := " ++ pprint t ++ "'."
                    ++ "A name, '" ++ pprint v' ++ "' for the term " ++ pprint t ++ " already exists in the environment:\n'" ++ pprint e ++ "'"

emptySys = System Map.empty

extSys n e j (System s)
  = let c = Constant n in
      case Map.lookup c s of
         (Just _) -> throwWithTrace
                          $ "Cannot declare the constant '" ++ pprint c ++"'.\n"
                         ++ "  The constant '" ++ pprint c ++ "' is alread defined in the system.\n"
                         ++ "  Currently defnied constants are: " ++ show (map pprint $ Map.keys s)
         Nothing -> return (System (Map.insert c (e,j) s),c)


definition n (System m)
 = let c = Constant n in
   case Map.lookup c m of
    Nothing -> throwWithTrace $ "No constant '" ++ pprint c ++ "' in the system." -- TODO: Improve message
    (Just x) -> return (c,x)


{- Pretty printing -}

instance PrettyPrint Constant where
   pprint (Constant n) = pprint n

instance PrettyPrint Term where
  pprint (Term h as) = unwords $ pprint h
                               : map (\(x,y) -> "{" ++ pprint x ++ " = " ++ pprint y ++ "}")
                                             (Map.toList as)
instance PrettyPrint Environment where
  pprint (Environment (Context args _) ds _)
      = commaList $ map (\(x,y) -> pprint x
                                ++ maybe "" (\t -> " := " ++ pprint t)
                                            (Map.lookup x ds)
                                ++ " : " ++ pprint y)
                                     $ Map.toList args



instance PrettyPrint System where
  pprint (System dfs) = unlines $ map (\(h,(c,jk)) -> unwords [pprint c, "⊢", pprint h," : ", pprint jk])
                                     $ Map.toList dfs
