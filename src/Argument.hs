{-# LANGUAGE FlexibleContexts #-}
{-|
    Module      : Argument
    Description : Data types for representing arguments for operations
                  and judgements.
    Stability   : experimental

This module defines data structures releated to
argument handling in Myott.
-}

module Argument (NamedArguments
                ,DependencyRelation
                ,Arguments(ArgList,ArgMap)
                ) where


import Data.Map.Strict (Map)

{- | 'NamedArguments' are arguments with specific names.
     for instance @f{x ↦ 3, y ↦ 2, name ↦ "foo"}@. -}
type NamedArguments name value
    = Map name value

{- | A 'DependencyRelation' is a graph where the edges
     represent some kind of dependency relation between
     the objects. In Myott these dependencies come
     from the depentencies between judgmentforms. -}
type DependencyRelation object index
    = Map object (Map index object)


{-|
    `Arguments` is either a list of arguments,or
    a mapping of argument identifiers to arguments.
-}
data Arguments argname a
   -- | An argument list of the form f(3,2,"foo")
   = ArgList [a]
   -- | Argument list of the form f{x ↦ 3, y ↦ 2, name ↦ "foo"}
   | ArgMap (NamedArguments argname a)
  deriving (Eq,Show)
