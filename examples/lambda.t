{
    ⊢ A : Type ;
    x : Element A ⊢ B : Type ;
    ────────────────────────
    ⊢ Π : Type
}

{
    ⊢ A : Type ;
    x : Element A ⊢ B : Type ;
    x : Element A ⊢ b : Element B ;
    ⊢ p ≔ Π A B
    ────────────────────────
    ⊢ λ : Element p
}
