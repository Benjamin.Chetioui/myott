
⊢ ob sort ;

dom : ob,
codom : ob
⊢ hom dom codom sort ;

x : ob, y : ob,
lhs : hom x y,
rhs : hom x y
⊢ ≡ lhs rhs prop ;
